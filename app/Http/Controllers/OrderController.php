<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrderRequest;
use App\Models\Order;
use App\Services\IngredientService;
use App\Services\ProductService;

class OrderController extends Controller
{

    /**
     * Create new order and update the stock.
     *
     * @param StoreOrderRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreOrderRequest $request)
    {
        // 1. Accepts the order details from the request payload.
        $order_data = $request->validated();

        // 2. Persists the Order in the database.
        Order::create([
            'products' => json_encode($order_data['products']),
            'status' => Order::STATUS_ACCEPTED,
            'customer_id' => $request->user()->id
        ]);

        // 3. Updates the stock of the ingredients.
        ProductService::prepare($order_data['products']);

        // Ensure that en email is sent once the level of any of the ingredients reach below 50%
        IngredientService::checkStockConsumption();

        return response()->json([
            'success' => true,
            'message' => __('Order has been accepted, stay tuned!')
        ]);
    }
}
