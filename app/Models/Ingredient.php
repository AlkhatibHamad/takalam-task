<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'total_amount',
        'consumed_amount',
        'unit'
    ];


    /**
     * Check weather the merchant is previously alert or not.
     *
     * @return bool
     */
    public function isMerchantNotAlerted()
    {
        return is_null($this->merchant_alert_at);
    }

    /**
     * Mark the merchant as alerted for this ingredient.
     *
     * @return void
     */
    public function markAsAlerted()
    {
        $this->update([
            'merchant_alert_at' => now()
        ]);
    }

    /**
     * Reset merchant alert flag when the stock is refilled.
     *
     * @return void
     */
    public function resetMerchantAlert()
    {
        $this->update([
            'merchant_alert_at' => null
        ]);
    }
}
