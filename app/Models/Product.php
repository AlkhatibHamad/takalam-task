<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Product extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Ingredients relationship
     *
     * @return BelongsToMany
     */
    public function ingredients()
    {
        return $this->belongsToMany(Ingredient::class)->withPivot('amount', 'unit'); // ManyToMany relationship
    }

    /**
     * Cook a product and consume the ingredients
     *
     * @param int $quantity
     * @return void
     */
    public function cook(int $quantity)
    {
        foreach ($this->ingredients as $ingredient) {
            $amount_needed = $ingredient->pivot->amount;
            $ingredient->increment('consumed_amount', $amount_needed * $quantity);
        }
    }
}
