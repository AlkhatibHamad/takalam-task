<?php

namespace App\Services;

use App\Mail\IngredientConsumptionExceeded;
use Illuminate\Support\Facades\Mail;
use App\Models\Ingredient;
use App\Models\User;

class IngredientService
{

    /**
     * Check ingredient stock consumption.
     *
     * @return void
     */
    public static function checkStockConsumption ()
    {
        foreach (Ingredient::all() as $ingredient)
        {
            $consumption_precentage = $ingredient->consumed_amount / $ingredient->total_amount * 100;

            if ($consumption_precentage >= 50 and $ingredient->isMerchantNotAlerted())
            {
                self::alertMerchant($ingredient);
            }
        }
    }

    /**
     * Send alert email to the marchant
     *
     * @param Ingredient $ingredient
     * @return void
     */
    public static function alertMerchant(Ingredient $ingredient)
    {
        $merchant = User::where('type', 'merchant')->first();

        Mail::to($merchant)
            ->send(new IngredientConsumptionExceeded($ingredient));

        $ingredient->markAsAlerted();
    }
}
