<?php

namespace App\Services;

use App\Models\Product;

class ProductService
{
    /**
     * Prepare ordered products for cooking.
     *
     * @param array $ordered_products
     * @return void
     */
    public static function prepare(array $ordered_products)
    {
        foreach ($ordered_products as $ordered_product) {
            $product = Product::findOrFail($ordered_product['product_id']);
            $product->cook(quantity: $ordered_product['quantity']);
        }
    }
}
