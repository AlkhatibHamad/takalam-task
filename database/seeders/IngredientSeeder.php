<?php

namespace Database\Seeders;

use App\Models\Ingredient;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class IngredientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Ingredient::insert([
            [
                "name" => "Beef",
                "total_amount" => 20 * 100,
                "unit" => "g",
            ],
            [
                "name" => "Cheese",
                "total_amount" => 5 * 100,
                "unit" => "g",
            ],
            [
                "name" => "Onion",
                "total_amount" => 1 * 100,
                "unit" => "g",
            ],
        ]);
    }
}
