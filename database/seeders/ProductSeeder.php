<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $product = Product::create([
            'name' => 'Burger',
        ]);

        $product->ingredients()->attach([
            1 => ['amount' => 150, 'unit' => 'g'],
            2 => ['amount' => 30, 'unit' => 'g'],
            3 => ['amount' => 20, 'unit' => 'g'],
        ]);
    }
}
