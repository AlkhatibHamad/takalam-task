<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class StoreOrderTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test order endpoint is working properly.
     */
    public function test_order_endpoint_is_working(): void
    {
        $this->seed();

        $user = User::factory()->create();

        $response = $this->actingAs($user)
            ->postJson('api/order/', [
                "products" => [
                    [
                        "product_id" => 1,
                        "quantity" => 2
                    ]
                ]
            ]);

        $response->assertValid();

        $this->assertTrue($response['success']);
    }

    /**
     * Test order is persists in database.
     */
    public function test_order_is_persists_in_database(): void
    {
        $this->assertDatabaseHas('orders', [
            'id' => 1,
            'status' => 'accepted',
        ]);

    }

    /**
     * Test ingredient stock is consumed and updated.
     */
    public function test_ingredient_is_correctly_stored(): void
    {
        $this->assertDatabaseHas('ingredients', [
            'name' => 'Beef',
            'consumed_amount' => '300',
        ]);

        $this->assertDatabaseHas('ingredients', [
            'name' => 'Cheese',
            'consumed_amount' => '60',
        ]);

        $this->assertDatabaseHas('ingredients', [
            'name' => 'Onion',
            'consumed_amount' => '40',
        ]);
    }
}
